THRESHOLD = -500  # Set the threshold for HMM fraud detection
STATES = 5
CLUSTERS = 3
STEPS = 200
TEST_RANGE = 1000
