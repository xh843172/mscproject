import numpy as np
from hmmlearn import hmm  # Use hmmlearn library to implement HMM

class HMM:
    def __init__(self, n_states, n_possible_observations):
        self.n_states = n_states
        self.n_possible_observations = n_possible_observations
        # Use GaussianHMM to handle continuous data
        self.__model = hmm.GaussianHMM(n_components=n_states, n_iter=100)

    def train_model(self, X):
        print('HMM is training ...')
        self.__model.fit(X)
        print('HMM training completed.')

    def detect_fraud(self, X_train, X_test, threshold):
        # Compute the log-likelihood of the test sample
        log_likelihood = self.__model.score(X_test)
        # If the log-likelihood is less than the threshold, it is classified as fraud
        return log_likelihood < threshold
