import pandas as pd
import numpy as np

# Driver class for loading and processing the dataset
class Driver:
    def __init__(self, path):
        self.path = path
        # Read the csv file and drop the 'id' column
        self._data = pd.read_csv(path).drop(columns=['id'])
        print('Dataset loaded completely.')

    # Get the loaded data
    def get_data(self):
        return self._data

    # Split the dataset into training and testing sets
    def get_train_test_split(self, test_size=0.2):
        from sklearn.model_selection import train_test_split
        # X is the feature set, dropping the 'Class' column; y is the target label, i.e., the 'Class' column
        X = self._data.drop('Class', axis=1)
        y = self._data['Class']
        return train_test_split(X, y, test_size=test_size, random_state=42)
