import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from Source.clustering import EnhancedKMeansClustering
from Source.driver import Driver
from Source.hidden_markov_model import HMM
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.decomposition import PCA
from config import *

# Solve the MiniBatchKMeans memory leak issue
os.environ["OMP_NUM_THREADS"] = "1"

def plot_correlation_heatmap(data, output_path):
    plt.figure(figsize=(10, 8))
    sns.heatmap(data.corr(), annot=False, cmap="coolwarm", linewidths=0.5)
    plt.title('Correlation Heatmap')
    plt.savefig(output_path)
    plt.close()

def plot_clusters(X_train, y_train_pred, output_path):
    pca = PCA(n_components=2)
    reduced_data = pca.fit_transform(X_train)

    plt.figure(figsize=(10, 8))
    plt.scatter(reduced_data[:, 0], reduced_data[:, 1], c=y_train_pred, cmap="viridis", s=10)
    plt.title('Cluster Scatter Plot')
    plt.xlabel('PCA Component 1')
    plt.ylabel('PCA Component 2')
    plt.colorbar(label='Cluster Label')
    plt.savefig(output_path)
    plt.close()

def plot_histogram(data, output_path):
    plt.figure(figsize=(10, 8))
    data.hist(bins=50, figsize=(20, 15))
    plt.suptitle('Histogram of Features')
    plt.savefig(output_path)
    plt.close()

def plot_model_evaluation(metrics, output_path, title):
    labels = list(metrics.keys())
    values = list(metrics.values())

    plt.figure(figsize=(10, 8))
    bars = plt.bar(labels, values, color='skyblue')

    # Add value labels
    for bar in bars:
        yval = bar.get_height()
        plt.text(bar.get_x() + bar.get_width()/2, yval + 0.01, round(yval, 3), ha='center', va='bottom')

    plt.title(title)
    plt.xlabel('Metrics')
    plt.ylabel('Score')
    plt.ylim(0, 1)
    plt.savefig(output_path)
    plt.close()

def evaluate_model(clustering_model, X_train, y_train, X_test, y_test, output_dir):
    print(f'Evaluating {clustering_model.__class__.__name__} ...')

    try:
        y_train_pred = clustering_model.run(X_train)
        y_test_pred = clustering_model.run(X_test)

        # Evaluate the model's performance
        accuracy = accuracy_score(y_test, y_test_pred)
        precision = precision_score(y_test, y_test_pred, average='weighted', zero_division=1)
        recall = recall_score(y_test, y_test_pred, average='weighted', zero_division=1)
        f1 = f1_score(y_test, y_test_pred, average='weighted', zero_division=1)

        metrics = {
            'Accuracy': accuracy,
            'Precision': precision,
            'Recall': recall,
            'F1 Score': f1
        }

        print(f"KMeans Clustering Performance:\n"
              f"Accuracy: {accuracy:.3f}\n"
              f"Precision: {precision:.3f}\n"
              f"Recall: {recall:.3f}\n"
              f"F1 Score: {f1:.3f}\n")

        # Save the evaluation chart
        plot_model_evaluation(metrics, os.path.join(output_dir, 'model_evaluation.png'), 'KMeans Model Evaluation')

        return y_train_pred, y_test_pred  # Return clustering results as input for HMM

    except Exception as e:
        print(f"Error evaluating {clustering_model.__class__.__name__}: {e}")

# HMM model evaluation function
def evaluate_hmm(train_data, test_data, y_test, output_dir):
    hmm_model = HMM(n_states=CLUSTERS, n_possible_observations=CLUSTERS)

    # Train the HMM model
    hmm_model.train_model(train_data)

    # Use the trained HMM model to detect fraud
    predictions = []
    for i in range(len(test_data)):
        fraud = hmm_model.detect_fraud(train_data, test_data[i].reshape(1, -1), THRESHOLD)
        predictions.append(1 if fraud else 0)

    # Evaluate the performance of the HMM model
    accuracy = accuracy_score(y_test, predictions)
    precision = precision_score(y_test, predictions, average='weighted', zero_division=1)
    recall = recall_score(y_test, predictions, average='weighted', zero_division=1)
    f1 = f1_score(y_test, predictions, average='weighted', zero_division=1)

    metrics = {
        'Accuracy': accuracy,
        'Precision': precision,
        'Recall': recall,
        'F1 Score': f1
    }

    print(f"HMM Fraud Detection Performance:\n"
          f"Accuracy: {accuracy:.3f}\n"
          f"Precision: {precision:.3f}\n"
          f"Recall: {recall:.3f}\n"
          f"F1 Score: {f1:.3f}\n")

    # Save the evaluation chart
    plot_model_evaluation(metrics, os.path.join(output_dir, 'hmm_evaluation.png'), 'HMM Model Evaluation')

if __name__ == '__main__':
    # Define the output directory
    output_dir = './visualizations'
    os.makedirs(output_dir, exist_ok=True)

    # Load the dataset
    driver = Driver('../Data/creditcard_2023.csv')
    X_train, X_test, y_train, y_test = driver.get_train_test_split()

    # Plot the correlation heatmap
    plot_correlation_heatmap(driver.get_data(), os.path.join(output_dir, 'correlation_heatmap.png'))

    # Initialize the enhanced KMeans clustering model, set batch_size to avoid memory leakage
    enhanced_kmeans = EnhancedKMeansClustering(n_clusters=CLUSTERS, use_pca=True, pca_components=10, batch_size=3072)

    # Evaluate the KMeans model and obtain clustering results
    train_clusters, test_clusters = evaluate_model(enhanced_kmeans, X_train, y_train, X_test, y_test, output_dir)

    # Plot the cluster scatter plot
    plot_clusters(X_train, train_clusters, os.path.join(output_dir, 'cluster_scatter_plot.png'))

    # Plot the histogram
    plot_histogram(driver.get_data(), os.path.join(output_dir, 'feature_histogram.png'))

    # Use the clustering results as input for HMM to perform fraud detection
    evaluate_hmm(train_clusters.reshape(-1, 1), test_clusters.reshape(-1, 1), y_test, output_dir)
