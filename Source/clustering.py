import numpy as np
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

# Enhanced KMeans clustering algorithm
class EnhancedKMeansClustering:
    def __init__(self, n_clusters=3, use_pca=True, pca_components=10, batch_size=100):
        self.n_clusters = n_clusters
        self.use_pca = use_pca
        self.pca_components = pca_components
        self.batch_size = batch_size
        self.scaler = StandardScaler()
        self.pca = PCA(n_components=self.pca_components) if self.use_pca else None
        self.model = MiniBatchKMeans(n_clusters=self.n_clusters, batch_size=self.batch_size, random_state=0)

    def run(self, data):
        # Standardize the data
        print('Standardizing data ...')
        data = self.scaler.fit_transform(data)

        # If using PCA for dimensionality reduction
        if self.use_pca:
            print(f'Reducing dimensionality to {self.pca_components} components using PCA ...')
            data = self.pca.fit_transform(data)

        # Perform Mini-Batch KMeans clustering
        print('Running Mini-Batch KMeans Clustering ...')
        labels = self.model.fit_predict(data)
        print('Clustering is finished.')
        return labels

    def predict(self, sample):
        # Standardize the new sample
        sample = self.scaler.transform([sample])

        # If using PCA for dimensionality reduction
        if self.use_pca:
            sample = self.pca.transform(sample)

        # Predict the cluster label for the sample
        return self.model.predict(sample)
